# ledger-docker-alpine

ledger-cli packaged as a Docker container using Alpine Linux

This is a microcontainer for Docker, containing the minimum required files to run [ledger-cli](https://www.ledger-cli.org/). This allows you to run the same version of docker executable on any platform that supports Docker.

The executable is set to ledger so any parameters passed to the docker run command will be passed to the ledger executable.

The image is published at Docker Hub: [ledger-docker-alpine](https://hub.docker.com/r/cick0/ledger-docker-alpine)

## Build

Clone the repository. Run

`docker build -t myledger .`

or replace "myledger" with any name you want to use for the image.

## Run

Example command with description:

`docker run --rm -v source:destination myledger -f file bal`

Options:

`--rm` = clean-up. Remove container's filesystem changes when the container exits.

`-v` = volume, shared filesystem. Specify "source:destination" to bind mount a volume. Mount the book location (source) on your host machine to your Docker VM (destination). This provides access from the Docker image to your local filesystem. 
For example `"$PWD"/data:/data`.

`myledger` = name of the image

Everything after "myledger" are the commands passed to ledger itself. Note that you *have to* send the "-f filepath" and that the filepath refers to the Docker image.
If you look at the `-v` description, it maps the ledger subfolder of your current folder to /data mount point. Then your ledger files would be accessible at `/data/journal.ledger`. 

## References

- [Docker Build](https://docs.docker.com/engine/reference/builder/)
- [Alpine Linux Packages](https://pkgs.alpinelinux.org/packages)
- [Microcontainers](https://medium.com/travis-on-docker/microcontainers-tiny-portable-docker-containers-1507e3bf8688)
- [ledger-cli](https://www.ledger-cli.org/)
